import Vue from 'vue'
import Router from 'vue-router'
import MainComponent from './components/MainComponent'

import AllBoxes from './components/AllBoxes'
import AddBox from './components/AddBox'
import SingleBox from './components/SingleBox'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: MainComponent
    },
    {
      path: '/all_boxes',
      name: 'AllBoxes',
      component: AllBoxes
    },
    {
      path: '/add_box',
      name: 'AddBox',
      component: AddBox
    },
    {
      path: '/box/:id',
      name: 'SingleBox',
      component: SingleBox
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
