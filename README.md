# Steps to run this Repo:

1.  `npm install` to install dependencies <br>
2. `npm run serve` to launch the server

## What Else is Included

1. Mock Backend Implementation.
2. Unit Testing with JEST.
3. E2E Testing with Cypress.

### Salient Features of the App:

1. View for Featured Customers
2. View for Normal Customers
3. Preview for addition of new STB customers.

### If you have any questions regarding running the repository, please contact me on singularity.javid@gmail.com